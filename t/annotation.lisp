(in-package :stagger/test)

;;; HACK depend on loaded example

(let ((org.example.route.api.stagger
       (asdf:system-relative-pathname :stagger "t/eg/api.lisp")))
  ;;; TODO introspect pckage from symbol, or possibly DEFPACAKGE form in source
  ;;; TODO move load form somewhere into ASDF
  (load org.example.route.api.stagger)
  (let ((package (find-package :org.example.route.api.stagger/0)))
    (plan 1)
    (ok package)

    (when package
      (plan 1)
      (ok 
       (stagger:api-symbols package)))))

(finalize)
  
