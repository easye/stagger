(ql:quickload :restas)

(restas:define-module #:org.example.route.api.stagger/0/0/1
  (:nicknames #:org.example.route.api.stagger/0/1
              #:org.example.route.api.stagger/0)
  (:use #:cl))

(in-package #:org.example.route.api.stagger/0)

(stagger:endpoint %farm/get ("farm/{id}"
                             :method :get
                             :content-type "text/plain")
                  ;; :args ((id :source :path))
                  ;; :responses ((200 :string)))
                  
  (format nil "Welcome to farm ~a!" id))

(stagger:endpoint %animal/get ("animal/{id}"
                               :method :get
                               :content-type "text/plain")
  ;; :args ((id :source :path))
  ;; :responses ((200 :string)))
  "Moo!")



