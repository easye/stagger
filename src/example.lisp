(restas:define-module #:route.example/0/0/1 
  (:nicknames #:route.example/0/0
              #:route.example/0)
  (:use #:cl))

(in-package #:route.example/0)

(stagger:endpoint %weather ("weather"
                            :doc "Retrieve current weather."
                            :content-type "text/plain")
  "Nice weather today")

(stagger:endpoint %farm ("animal"
                         :doc
                         "Retrieve ANIMAL for FARMER from BARN."

                         :args
                         ((animal :source :parameter)
                          (farmer :source :query)
                          (barn   :source :parameters))

                         :responses
                         ((200 (:parenscript
                                (ps:create type "animal"
                                           data "{0}")))
                          (304 
                             :doc "No such animinal in this barn ID.  See another barn."
                             (:uri "/barn/{id}"))
                          ((< 400 499) (:content-type "text/plain")))

                         :content-type
                         "text/plain")
  (format nil "Old MacDonald has a ~a on his farm." animal))

;;; TODO: transcription of API grouping metadata (baseURI, author, revision, etc.)
